package ai.ecma.appregion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppRegionApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppRegionApplication.class, args);
    }

}
